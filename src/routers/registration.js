const express = require('express')
const router = express.Router()
const sql = require('mssql')
const con = require('../database.js')
router.get('/register', (req, res) => {
    con.query("SELECT reg_id, preperso_cid, preperso_laser_no ,register_rcode, subrcode,DATE_FORMAT(update_create_date,'%d %M %Y') AS 'update_create_date',update_create_time, update_create_by, status, notation, preperso_box_pre_id FROM registration", (err, rows, fields) => {
      if (err) {
        res.status(500).send('Error')
      } else {
        res.send(rows)
      }
    })
  })
  router.get('/register/:id', (req, res) => {
    con.query('SELECT * FROM registration WHERE reg_id = ?', [req.params.id], (err, rows, fields) => {
      if (err) {
        res.status(500).send('Error')
      } else {
        res.send(rows)
      }
    })
  })
  router.get('/registerid/:id', (req, res) => {
    con.query('SELECT reg_id FROM registration WHERE reg_id = ?', [req.params.id],(err, rows, fields) => {
      if (!err) {
        res.send(rows)
      } else {
        res.status(500).send('Error')
      }
    })
  })
  router.get('/reg-status/:id', (req, res) => {
    con.query('SELECT status FROM registration WHERE status = ?', [req.params.id],(err, rows, fields) => {
      if (!err) {
        res.send(rows)
      } else {
        res.status(500).send('Error')
      }
    })
  })
  router.delete('/register/:id', (req, res) => {
    con.query('DELETE FROM registration WHERE reg_id = ?', [req.params.id], (err, rows, fields) => {
      if (!err) {
        res.send('delete')
      } else {
        res.status(500).send('Error')
      }
    })
  })
  router.post('/register',(req,res) => {
    con.query('INSERT INTO registration SET ?',req.body,(err,result) => {
      console.log(req.body)
      if(err) {
        res.status(500).send('Error')
      }else {
      res.status(201).send(`status ID: ${result.preperso_cid}`);
      }
    });
  });

  router.put('/register/:id',(req,res) => {
    const id = req.params.id;
    con.query('UPDATE `registration` SET `CID` = ?,`laser_no` = ?, `register_rcode` = ?, `sub_rcode` = ?,`update_create_date` = ?, `update_create_time` = ?, `update_create_by` = ?, `status` = ?, `notation` = ?, `box_pre_id` = ? WHERE reg_id = ?',[req.body.CID,req.body.laser_no,req.body.register_rcode,req.body.sub_rcode,req.body.update_create_date,req.body.update_create_time,req.body.update_create_by,req.body.status,req.body.notation,req.body.box_pre_id,req.body.reg_id], (err,result) => {
      if(err){
        res.status(500).send('Error')
      } else {
        res.status(201).send(JSON.stringify(result))
      }
      
    });
  });
module.exports = router;
