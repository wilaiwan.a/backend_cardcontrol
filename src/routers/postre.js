const express = require('express')
const router = express.Router()
const fs = require('fs')
const csv = require('csvjson')
const con = require('../database.js')
// var xlsx = require('node-xlsx').default;
var xlsxjson = require('xlsx-to-json')
var xlsjson = require('xls-to-json')

router.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.header('Access-Control-Allow-Methods','POST, PUT, OPTION, DELETE, GET');
    res.header('Access-Control-Max-Age','3600');
    res.header('Access-Control-Allow-Headers','Content-Type,Access-Control-Allow-Headers,Authorization, X-Request')
    next();
})
router.use(express.static(__dirname + '/public'));
router.use('/public/uploads',express.static(__dirname + '/public/uploads'));

router.post('/api',(req,res)=>{
    xlsxjson({
        input: './preperso.xlsx',
        output:"output.json",
        lowerCaseHeaders:true        
    },(err, result)=>{
        if(err){
            res.send(err);
        }else{
            res.json(result);
            con.query('INSERT INTO preperso SET `sublot` = ?,`cid` = ?, `laser_no` = ?, `file_name` = ?, `create_date` = ?, `create_time` = ?, `create_by` = ?, `status` = ?, `box_pre_id` = ?',[req.body.sublot,req.body.cid,req.body.lasser_no,req.body.file_name,req.body.create_date,req.body.create_time,req.body.create_by,req.body.status,req.body.box_pre_id],(err,result) => {
                if(!err) {
                    res.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
                    console.log(req.body)
                }
                
            })
        }
 });
        
        
});
module.exports = router;