const express = require('express')
const router = express.Router()

const con = require('../database.js')

router.get('/deliver', (req, res) => {
  con.query("SELECT deliver_id, preperso_box_pre_idx, sender_rcode, DATE_FORMAT(sender_create_date,'%d %M %Y') AS 'sender_create_date',sender_create_time,sender_create_by, receiver_rcode,DATE_FORMAT(receiver_create_date,'%d %M %Y') AS 'receiver_create_date', receiver_create_time,receiver_create_by,number_of_card,status,notation FROM deliver", (err, rows, fields) => {
    if (err) {
      res.status(500).send('Error')
    } else {
      res.send(rows)
    }
  })
})
router.get('/deliverid/:id', (req, res) => {
  con.query('SELECT deliver_id FROM deliver WHERE deliver_id = ?',[req.params.id], (err, rows, fields) => {
    if (err) {
      res.status(500).send('Error')
    } else {
      res.send(rows)
    }
  })
})
router.get('/deliver-status/:id', (req, res) => {
  con.query('SELECT status FROM deliver WHERE status = ?', [req.params.id],(err, rows, fields) => {
    if (err) {
      res.status(500).send('Error')
    } else {
      res.send(rows)
    }
  })
})
router.get('/deliver/:id', (req, res) => {
  con.query('SELECT * FROM deliver WHERE deliver_id = ?', [req.params.id], (err, rows, fields) => {
    if (err) {
      res.status(500).send('Error')
    } else {
      res.send(rows)
    }
  })
})
router.get('/rec', (req, res) => {
  con.query("SELECT deliver_id, sender_rcode, sender_rcode, DATE_FORMAT(sender_create_date,'%d %M %Y') AS 'sender_create_date', receiver_rcode, status FROM deliver WHERE status = 'S011' GROUP BY deliver_id ",(err, rows, fields) => {
    if (err) {
      res.status(500).send('Error')
    } else {
      res.send(rows)
    }
  })
})

router.delete('/deliver/:id', (req, res) => {
  con.query('DELETE FROM deliver WHERE deliver_id = ?', [req.params.id], (err, rows, fields) => {
    if (!err) {
      res.send('delete')
    } else {
      res.status(500).send('Error')
    }
  })
})

router.post('/deliver',(req,res) => {
  con.query('INSERT INTO deliver SET ?',req.body,(err,result) => {
    if(err) {
      res.status(500).send('Error')
    }else {
    res.status(201).send(`auction ID: ${result.deliver_id}`);
    }
  });
});


router.put('/deliver/:id',(req,res) => {
  const id = req.params.id; 
  console.log(req.body)
  con.query('UPDATE `deliver` SET `receiver_create_date` = ?, `receiver_create_time` = ?, `receiver_create_by` = ?, `status` = ? WHERE deliver_id = ?',[req.body.receiver_create_date,req.body.receiver_create_time,req.body.receiver_create_by,req.body.status,id], (err,result) => {
    console.log(id);
    if(err){
      res.status(500).send('Error')
    } else {
      res.status(201).send(JSON.stringify(result))
    }
    
  });
});

module.exports = router
