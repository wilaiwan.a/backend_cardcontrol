const express = require('express')
const router = express.Router()

const con = require('../database.js')


router.get('/preperso', (req, res) => {
  con.query("SELECT cid, laser_no, file_name,DATE_FORMAT(create_date,'%d %M %Y') AS 'create_date', create_time, create_by, status, box_pre_id  FROM preperso", (err, rows, fields) => {
    if (!err) {
      res.send(rows)
    } else {
      res.status(500).send('Error')
    }
  })
})

router.get('/preperso-cid', (req, res) => {
  con.query('SELECT cid FROM preperso', (err, rows, fields) => {
    if (!err) {
      res.send(rows)
    } else {
      res.status(500).send('Error')
    }
  })
})
router.get('/preperso-laserno', (req, res) => {
  con.query('SELECT laser_no FROM preperso', (err, rows, fields) => {
    if (!err) {
      res.send(rows)
    } else {
      res.status(500).send('Error')
    }
  })
})

router.get('/preperso-status', (req, res) => {
  con.query('SELECT status FROM preperso', (err, rows, fields) => {
    if (!err) {
      res.send(rows)
    } else {
      res.status(500).send('Error')
    }
  })
})

router.get('/preperso/:id', (req, res) => {
  con.query('SELECT * FROM preperso WHERE cid = ?', [req.params.id], (err, rows, fields) => {
    if (!err) {
      res.send(rows)
    } else {
      res.status(500).send('Error')
    }
  })
})

router.delete('/preperso/:id', (req, res) => {
  con.query('DELETE FROM preperso WHERE cid = ?', [req.params.id], (err, rows, fields) => {
    if (!err) {
      res.send('delete')
    } else {
      res.status(500).send('Error')
    }
  })
})

router.post('/preperso',(req,res) => {
  con.query('INSERT INTO preperso SET ?',req.body,(error,result) => {
    if(error) {
      res.status(500).send('Error')
    }else {
    res.status(201).send(`preperso ID: ${result.cid}`);
    }
  });
});
module.exports = router


// post 
// if(error) throw error;

//     res.status(201).send(`ID: ${result.cid}`);