const express = require('express')
const router = express.Router()
const con = require('../database.js')
  
  router.get('/auction', (req, res) => {
    con.query("SELECT auction_id,DATE_FORMAT(create_date,'%d %M %Y') AS 'create_date' , create_time, create_by ,number_of_card, price FROM auction", (err, rows, fields) => {
      if (err) {
        res.status(500).send('Error')
      } else {
        res.send(rows)
      }
    })
  })
  router.get('/auctionid/:id', (req, res) => {
    con.query('SELECT auction_id FROM auction WHERE auction_id = ?', [req.params.id],(err, rows, fields) => {
      if (!err) {
        res.send(rows)
      } else {
        res.status(500).send('Error')
      }
    })
  })
 
  router.get('/auction/:id', (req, res) => {
    con.query('SELECT * FROM auction WHERE auction_id = ?', [req.params.id], (err, rows, fields) => {
      if (err) {
        res.status(500).send('Error')
      } else {
        res.send(rows)
      }
    })
  })

router.delete('/auction/:id', (req, res) => {
  con.query('DELETE FROM auction WHERE auction_id = ?', [req.params.id], (err, rows, fields) => {
    if (!err) {
      res.send('delete')
    } else {
      res.status(500).send('Error')
    }
  })
})

router.post('/auction',(req,res) => {
  con.query('INSERT INTO auction SET ?',req.body,(err,result) => {
    if(err) {
      res.status(500).send('Error')
    }else {
    res.status(201).send(`auction ID: ${result.auction_id}`);
    }
  });
});

module.exports = router;
