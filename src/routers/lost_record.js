const express = require('express')
const router = express.Router()

const con = require('../database.js')

router.get('/lostRecord', (req, res) => {
  con.query("SELECT CONCAT(lost_record_id,'000',sublost) AS 'lost_record_id',sublost, deliver_id, sender_rcode, DATE_FORMAT(sender_create_date,'%d %M %Y') AS 'sender_create_date',sender_create_time,sender_create_by, receiver_rcode,DATE_FORMAT(receiver_create_date,'%d %M %Y') AS 'receiver_create_date', receiver_create_time,receiver_create_by,status,notation  FROM lost_record", (err, rows, fields) => {
    if (!err) {
      res.send(rows)
    } else {
      console.log(err)
    }
  })
})
router.get('/lostRecordid', (req, res) => {
  con.query('SELECT lost_record_id FROM lost_record', (err, rows, fields) => {
    if (!err) {
      res.send(rows)
    } else {
      console.log(err)
    }
  })
})
router.get('/lost-deliverid', (req, res) => {
  con.query('SELECT deliver_id FROM lost_record', (err, rows, fields) => {
    if (!err) {
      res.send(rows)
    } else {
      console.log(err)
    }
  })
})
router.get('/lost-status', (req, res) => {
  con.query('SELECT status FROM lost_record', (err, rows, fields) => {
    if (!err) {
      res.send(rows)
    } else {
      console.log(err)
    }
  })
})

router.get('/lostRecord/:id', (req, res) => {
  con.query('SELECT * FROM lost_record WHERE lost_record_id = ?', [req.params.id], (err, rows, fields) => {
    if (!err) {
      res.send(rows)
    } else {
      console.log(err)
    }
  })
})
router.get('/lostR', (req, res) => {
  con.query("SELECT CONCAT(lost_record_id,'000',sublost) AS 'lost_record_id',sublost,notation, sender_rcode, sender_rcode, DATE_FORMAT(sender_create_date,'%d %M %Y') AS 'sender_create_date', receiver_rcode, status FROM lost_record WHERE status = 'S011' GROUP BY sublost ",(err, rows, fields) => {
    if (!err) {
      res.send(rows)
    } else {
      console.log(err)
    }
  })
})
router.delete('/lostRecord/:id', (req, res) => {
    con.query('DELETE FROM lost_record WHERE lost_record_id = ?', [req.params.id], (err, rows, fields) => {
      if (!err) {
        res.send('delete')
      } else {
        console.log(err)
      }
    })
  })
  
  router.post('/lostRecord',(req,res) => {
    con.query('INSERT INTO lost_record SET ?',req.body,(err,result) => {
      console.log(req.body)
      if(err) {
        res.status(500).send('Error')
      }else {  
      res.status(201).send(`User add ID: ${result.lost_record_id}`);
      }
    });
  });
  router.post('/lostRe',(req,res) => {
    con.query('INSERT INTO lost_record SET `lost_record_id` = ?, `deliver_id` = ?, `sender_rcode` = ?, `sender_create_date` = ?, `sender_create_time` = ?, `sender_create_by` = ?',[req.body.lost_record_id,req.body.deliver_id,req.body.sender_rcode,req.body.sender_create_date,req.body.sender_create_time,req.body.sender_create_by],(err,result) => {
      console.log(req.body)
      if(err) {
        res.status(500).send('Error')
      }else {  
      res.status(201).send(`User add ID: ${result.lost_record_id}`);
      }
    });
  });

  router.put('/lostRecord/:id',(req,res) => {
    const id = req.params.id;
    con.query("UPDATE `lost_record` SET  `receiver_create_date` = ?, `receiver_create_time` = ?, `receiver_create_by` = ?, `status` = ? WHERE sublost = ? ",[req.body.receiver_create_date,req.body.receiver_create_time,req.body.receiver_create_by,req.body.status,id], (err,result) => {
      console.log(req.body)
      if(err){
        res.status(500).send('Error')
      } else {
        res.status(201).send(JSON.stringify(result))
      }
    });
  });


module.exports = router
