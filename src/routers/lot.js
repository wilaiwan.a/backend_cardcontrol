const express = require('express')
const router = express.Router()

const con = require('../database.js')

router.get('/lotid', (req, res) => {
  con.query('SELECT lot_id FROM lot', (err, rows, fields) => {
    if (!err) {
      res.send(rows)
    } else {
      res.status(500).send('Error')
    }
  })
})
router.get('/lot-auction/:id', (req, res) => {
  con.query("SELECT CONCAT(auction_id,'000',sublot) AS 'lot_id',auction_id, DATE_FORMAT(create_date,'%d %M %Y') AS 'create_date',create_time,create_by,number_of_box,number_of_card FROM lot WHERE auction_id = ?",[req.params.id], (err, rows, fields) => {
    if (!err) {
      res.send(rows)
    } else {
      res.status(500).send('Error')
    }
  })
})

router.get('/lot/:id', (req, res) => {
  con.query('SELECT * FROM lot WHERE lot_id = ?', [req.params.id], (err, rows, fields) => {
    if (!err) {
      res.send(rows)
    } else {
      res.status(500).send('Error')
    }
  })
})

router.delete('/lot/:id', (req, res) => {
  con.query('DELETE FROM lot WHERE lot_id = ?', [req.params.id], (err, rows, fields) => {
    if (!err) {
      res.send('delete')
    } else {
      res.status(500).send('Error')
    }
  })
})
router.post('/lot',(req,res) => {
  con.query('INSERT INTO lot SET ?',req.body,(err,result) => {
    if(err) {
      res.status(500).send('Error')
    }else {
    res.status(201).send(`lot ID: ${result.lot_id}`);
    }
  });
});

router.get('/group', (req, res) => {
  con.query("SELECT auction_id, SUM (number_of_box) AS 'number_of_box',SUM (number_of_card) AS 'number_of_card' FROM lot GROUP BY auction_id", (err, rows, fields) => {
    if (!err) {
      res.send(rows)
    } else {
      res.status(500).send('Error')
    }
  })
})
router.get('/gr/:id', (req, res) => {
  con.query("SELECT auction_id, SUM (number_of_box) AS 'number_of_box',SUM (number_of_card) AS 'number_of_card' FROM lot WHERE auction_id = ? GROUP BY auction_id",[req.params.id], (err, rows, fields) => {
    if (!err) {
      res.send(rows)
    } else {
      res.status(500).send('Error')
    }
  })
})


module.exports = router
