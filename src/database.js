const mysql = require('mysql')

var con = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  database: 'card',
  multipleStatements: true,
  dateStrings: 'date'
})

con.connect((err) => {
  if (!err) {
    console.log('connect succed.')
  } else {
    console.log('connect failed \n Error : ' + JSON.stringify(err, undefined, 2))
  }
})
module.exports = con
