const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const cors = require('cors')
app.locals.moment = require('moment')

app.set('port', process.env.PORT || 3000)

app.use(express.json())
app.use(cors())

app.use(require('./routers/auction'))
app.use(require('./routers/deliver'))
app.use(require('./routers/lost_record'))
app.use(require('./routers/lot'))
app.use(require('./routers/preperso'))
app.use(require('./routers/registration'))
app.use(bodyParser.json())

app.listen(app.get('port'), () => {
  console.log(`Server on port ${app.get('port')}`)
})
